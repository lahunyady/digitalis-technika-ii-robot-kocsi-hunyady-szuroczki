#define A_MotorEnable 2
#define B_MotorEnable 7
#define A_MotorDir1 3 //Jobb motor 
#define A_MotorDir2 4
#define B_MotorDir1 5 //Bal motor
#define B_MotorDir2 6
#define LED0 12  // Jobb szelso LED
#define LED1 11
#define LED2 10 //Kozepso LED
#define LED3 9
#define LED4 8 //Bal szelso LED
#define LEFT_BTN 19
#define RIGHT_BTN 18
#define SONAR_TRIGGER_PIN 44 //zold
#define SONAR_ECHO_PIN 45    //sarga

boolean bekapcsolva = false;




const int szuroMeret = 5;
int szuresiIdo = 400;
unsigned long szuresIdo=0;
int szuresIdoKoz = szuresiIdo/szuroMeret;
int szuro[szuroMeret];

int irPins[5] = {A1, A2, A3, A4, A5};
int sensorState = 0;
const int hatarertek = 400;

double error = 0;
double errorLast = 0;
const int maxPwm = 60;
const int minPwm = 0;
const double Kp = 15;
const double Kd = 5;

int akadaly = 0;

unsigned long kereszt= 0;
unsigned long jobbraVoltJelzes=0;
unsigned long balraVoltJelzes=0;
unsigned long dTimer = 0;
unsigned long savValtIdo = 700;
unsigned long utolsoSonarOlvasas = 0;

int prevMain = -1;
double savValtEro = 1;
int sum = 0;
bool balraVolt =false;
bool jobbraVolt =false;
int rpmBT;

//BT
unsigned long lastSend = 0;
int state = 1;

//Sonar
long sonarDuration, sonarDistance;

bool mintatTalal(int keresendo){
  for(int i=0; i<szuroMeret; i++){
    if(szuro[i]!= keresendo){
      return false;
    }
  }
  return true;
}

void mintaRogizt(int rogzitendo){
    for (int j=szuroMeret-1; j>0; j--){
      szuro[j] = szuro[j-1];
    }
    szuro[0] = rogzitendo;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(A_MotorDir1, OUTPUT);
  pinMode(A_MotorDir2, OUTPUT);
  pinMode(B_MotorDir1, OUTPUT);
  pinMode(B_MotorDir2, OUTPUT);
  pinMode(A_MotorEnable, OUTPUT);
  pinMode(B_MotorEnable, OUTPUT);

  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);

  pinMode(RIGHT_BTN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RIGHT_BTN), kapcsolas, FALLING);

  pinMode(SONAR_TRIGGER_PIN, OUTPUT);
  pinMode(SONAR_ECHO_PIN, INPUT);

  Serial.begin(9600);
  Serial2.begin(9600);

}
void loop() {
if(lastSend + 100 < millis()){
  lastSend += 100;

    Serial2.print("#");
    Serial2.print("a");
    Serial2.print(bekapcsolva);
    
    Serial2.print("b");
    Serial2.print(error);
    
    Serial2.print("c");
    Serial2.print(errorLast);
    
    Serial2.print("d");
    Serial2.print(bitRead(prevMain,4));
    Serial2.print(bitRead(prevMain,3));
    Serial2.print(bitRead(prevMain,2));
    Serial2.print(bitRead(prevMain,1));
    Serial2.print(bitRead(prevMain,0));
    Serial2.println(prevMain);
    
    Serial2.print("e");
    Serial2.print(balraVolt);
    
    Serial2.print("f");
    Serial2.print(jobbraVolt);
    
    Serial2.print("g");
    Serial2.print(akadaly);
    
    Serial2.print("h");
    Serial2.print(bitRead(sensorState,4));
    Serial2.print(bitRead(sensorState,3));
    Serial2.print(bitRead(sensorState,2));
    Serial2.print(bitRead(sensorState,1));
    Serial2.print(bitRead(sensorState,0));

    Serial2.print("~");
}

if(Serial2.available()){
    char data = Serial2.read();

    switch (data){
      case 'l':
        setBTLSpeed();
      break;
      case 'r':
        setBTRSpeed();
      break;
      case 'm':
        state = 2;
      break;
      case 'n':
        state = 1;
      break;
      default:
      rpmBT*=10;
      rpmBT += data - '0';
      break;
    }
  }

  sonarOlvas();
  
  IrOlvas();  
  if(bekapcsolva && state == 1){
    kovet();
    }
    
}

void setBTRSpeed(){
  Serial.print("R SPEED  -  ");
  Serial.println(rpmBT);
  if(rpmBT < 140 ){
    jobbMotor(rpmBT);
  }
  rpmBT = 0;
}

void setBTLSpeed(){
  Serial.print("L SPEED  -  ");
  Serial.println(rpmBT);
  if(rpmBT < 140 ){
    balMotor(rpmBT);
  }
  rpmBT = 0;
}



void kovet(){
  errorLast = error;
  switch (sensorState){
    case B00001: error = 2.5; prevMain = 1;  break; //8
    case B00010: 
      error = 2.5; 
      prevMain = 2;
      if(mintatTalal(sensorState)){
        jobbraVolt = false; 
        balraVolt = false; break; //8
      }
      break;
        
    case B00100: 
      error = 0; 
      prevMain = 4; 
        jobbraVolt = false; 
        balraVolt = false; break; //8
    break;  //4
    
    case B01000: 
      error = -2.5; 
      prevMain = 8; 
      if(mintatTalal(sensorState)){
        jobbraVolt = false; 
        balraVolt = false; break; //8
      }
   
    break; //8
    
    case B10000: error = -2.5; prevMain = 16;  break; //16
    
    case B00011: error = 2.5; savValtEllenoriz(); break;  //3
    case B00110: error = 0; savValtEllenoriz(); break;  //6
    case B01100: error = 0; savValtEllenoriz(); break; //12
    case B11000: error = -2.5; savValtEllenoriz(); break; //24
    
    case B00111:
    case B01110:
    error = 1;
    case B11100:
    savValtEllenoriz();
    break;
    
    
    case B01111:
    case B11110:
    case B11111: 
        error = 0;
        kereszt = millis();  
    break; 

    case B00000:
    if(mintatTalal(sensorState)){
      uresVan();
    }

    break;
    default:
    error = 1.3;
    break;
  }
  if(bekapcsolva){
    meghajt();
  }
}
void savValtEllenoriz(){
  if(mintatTalal(sensorState)){
    if(jobbraVoltJelzes + savValtIdo > millis()){
      jobbraVoltJelzes = millis();
    } 
    else if (balraVoltJelzes + savValtIdo > millis()) {
        balraVoltJelzes = millis();
    }
    else if(sensorState == B00011 && prevMain == B00001 && error <0){
      jobbraVoltJelzes = millis();
    }
    else if(sensorState == B11000 && prevMain == B10000 && error > 0){
      balraVoltJelzes = millis();
    }
    else if (sensorState - prevMain < prevMain){
      if(szuro[1] == 4 && szuro[2] == 4 && error < 0){
        balraVoltJelzes = millis();  
      }else{
        jobbraVoltJelzes = millis();
      }
    }
    else if (sensorState - prevMain > prevMain){
      if(szuro[1] == 4 && szuro[2] == 4  && error > 0){
        jobbraVoltJelzes = millis();
      } else{
        
        balraVoltJelzes = millis();  
      }
    }else if(error < 0 || errorLast < 0){
        jobbraVoltJelzes = millis();  
    } else {
      balraVoltJelzes = millis();
    }
  }
}


void uresVan(){
  if(jobbraVoltJelzes + savValtIdo > millis() ){
    errorLast = error;
    error = savValtEro*0.62;
    jobbraVolt = true;
  } else if (balraVoltJelzes + savValtIdo > millis() ){
    errorLast = error;
    error = -savValtEro*0.4;
    balraVolt = true;

  } else if(kereszt + 1500 > millis() && !balraVolt && !jobbraVolt){
    kikapcs();
  }
}

void kikapcs(){
  analogWrite(A_MotorEnable, 0); //jobb
  analogWrite(B_MotorEnable, 0);  //bal
  digitalWrite(A_MotorDir1, LOW);
  digitalWrite(A_MotorDir2, LOW);
  digitalWrite(B_MotorDir1, LOW);
  digitalWrite(B_MotorDir2, LOW);
  bekapcsolva = false;
}


void meghajt(){
  
    double actualError = Kp * error + Kd*(error-errorLast);
    balMotor(53 + actualError);
    jobbMotor(53 - actualError);
  
}

void jobbMotor(int pwm){
  if(pwm > maxPwm){pwm = maxPwm;}
  if(pwm < minPwm) {pwm = 0;}
  analogWrite(A_MotorEnable, pwm); //jobb
  digitalWrite(A_MotorDir1, HIGH);
  digitalWrite(A_MotorDir2, LOW);
  
}

void balMotor(int pwm){
  if(pwm > maxPwm){pwm = maxPwm;}
  if(pwm < minPwm) {pwm = 0;}
  analogWrite(B_MotorEnable, pwm);  //bal
  digitalWrite(B_MotorDir1, HIGH);
  digitalWrite(B_MotorDir2, LOW);
}

void kapcsolas(){
  if (bekapcsolva) {
    bekapcsolva = false; 
    analogWrite(A_MotorEnable,0);   //Left Motor Speed
    analogWrite(B_MotorEnable,0);  //Right Motor Speed
  }
  else{
    bekapcsolva = true;    
  }
}

void sonarOlvas() {
  if (utolsoSonarOlvasas + 200 < millis()) {
    utolsoSonarOlvasas = millis();
    digitalWrite(SONAR_TRIGGER_PIN, LOW);
    delayMicroseconds(2);
  
    digitalWrite(SONAR_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
  
    digitalWrite(SONAR_TRIGGER_PIN, LOW);
    sonarDuration = pulseIn(SONAR_ECHO_PIN, HIGH);
  
    if(sonarDuration >= 11640){ //58.2 * 200(max distance)
      akadaly = 0;
    }
    else {
      sonarDistance = sonarDuration / 58.2;
      akadaly = sonarDistance;
    }
  }   
}

void IrOlvas()
{
  boolean tempSens[5];

  tempSens[0] = analogRead(irPins[0]) > hatarertek ? 1 : 0; //jobb szélső
  tempSens[1] = analogRead(irPins[1]) > hatarertek ? 1 : 0;
  tempSens[2] = analogRead(irPins[2]) > hatarertek ? 1 : 0; //kozepso
  tempSens[3] = analogRead(irPins[3]) > hatarertek ? 1 : 0;
  tempSens[4] = analogRead(irPins[4]) > hatarertek ? 1 : 0; //bal szélső

  atalakit(tempSens);
}

void atalakit(boolean tempSens[5]) {
  sum=0;
  sensorState = 0;
  for (int i=0;i<5;i++){
    int temp = tempSens[i];
    sum += temp;
    digitalWrite(LED0 - i, tempSens[i]);
    sensorState = sensorState + (tempSens[i]<<i);
    
  }

  

  if(szuresIdo + szuresIdoKoz < millis()){
    szuresIdo = millis();
    mintaRogizt(sensorState);
  }
}

