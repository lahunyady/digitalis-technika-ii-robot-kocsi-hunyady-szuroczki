#include <Wire.h>
#include <TimerOne.h>

#define    MPU9250_ADDRESS            0x68
#define    MAG_ADDRESS                0x0C

#define    GYRO_FULL_SCALE_250_DPS    0x00  
#define    GYRO_FULL_SCALE_500_DPS    0x08
#define    GYRO_FULL_SCALE_1000_DPS   0x10
#define    GYRO_FULL_SCALE_2000_DPS   0x18

#define    ACC_FULL_SCALE_2_G        0x00  
#define    ACC_FULL_SCALE_4_G        0x08
#define    ACC_FULL_SCALE_8_G        0x10
#define    ACC_FULL_SCALE_16_G       0x18

#define A_MotorEnable 2
#define B_MotorEnable 7
#define A_MotorDir1 3 //Jobb motor 
#define A_MotorDir2 4
#define B_MotorDir1 5 //Bal motor
#define B_MotorDir2 6
#define LED0 12  // Jobb szelso LED
#define LED1 11
#define LED2 10 //Kozepso LED
#define LED3 9
#define LED4 8 //Bal szelso LED
#define LEFT_BTN 19
#define RIGHT_BTN 18
#define SONAR_TRIGGER_PIN 44 //zold
#define SONAR_ECHO_PIN 45    //sarga

boolean bekapcsolva = false;

// Counter
long int cpt=0;

// Initial time
long int ti;
volatile bool intFlag=false;





const int szuroMeret = 5;
int szuresiIdo = 400;
unsigned long szuresIdo=0;
int szuresIdoKoz = szuresiIdo/szuroMeret;
int szuro[szuroMeret];

int irPins[5] = {A1, A2, A3, A4, A5};
int sensorState = 0;
const int hatarertek = 400;

double error = 0;
double errorLast = 0;
const int maxPwm = 60;
const int minPwm = 0;
const double Kp = 15;
const double Kd = 5;

int akadaly = 0;

unsigned long startTime = 0;
unsigned long endTime = 0;
unsigned long stateChangeTime = 0;

unsigned long kereszt= 0;
unsigned long jobbraVoltJelzes=0;
unsigned long balraVoltJelzes=0;
unsigned long dTimer = 0;
unsigned long savValtIdo = 700;
unsigned long utolsoSonarOlvasas = 0;
int elsoCelIrany = 0;

int prevMain = -1;
double savValtEro = 1;
int sum = 0;
bool balraVolt =false;
bool jobbraVolt =false;
int rpmBT;

//BT
unsigned long lastSend = 0;
int state = 1;

//Sonar
long sonarDuration, sonarDistance;


double dir = 0;
int dirInt = 0;
int avgVal[5] = {0,0,0,0,0};
int avgDir = 0;
int celIrany = 0;
int errorMagnitude = 0;
double errorMagnitudeLast = 0;

int magnitudeGep = 0;

bool keneOlvasni = true;



bool mintatTalal(int keresendo){
  for(int i=0; i<szuroMeret; i++){
    if(szuro[i]!= keresendo){
      return false;
    }
  }
  return true;
}

void mintaRogizt(int rogzitendo){
    for (int j=szuroMeret-1; j>0; j--){
      szuro[j] = szuro[j-1];
    }
    szuro[0] = rogzitendo;
}

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
  
  pinMode(A_MotorDir1, OUTPUT);
  pinMode(A_MotorDir2, OUTPUT);
  pinMode(B_MotorDir1, OUTPUT);
  pinMode(B_MotorDir2, OUTPUT);
  pinMode(A_MotorEnable, OUTPUT);
  pinMode(B_MotorEnable, OUTPUT);

  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);

  pinMode(RIGHT_BTN, INPUT_PULLUP);
  pinMode(LEFT_BTN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RIGHT_BTN), kapcsolas, FALLING);
  attachInterrupt(digitalPinToInterrupt(LEFT_BTN), magniKapcsolas, FALLING);

  pinMode(SONAR_TRIGGER_PIN, OUTPUT);
  pinMode(SONAR_ECHO_PIN, INPUT);

  Serial.begin(9600);
  Serial2.begin(9600);

  // Set accelerometers low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,29,0x06);
  // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,26,0x06);
 
  
  // Configure gyroscope range
  I2CwriteByte(MPU9250_ADDRESS,27,GYRO_FULL_SCALE_1000_DPS);
  // Configure accelerometers range
  I2CwriteByte(MPU9250_ADDRESS,28,ACC_FULL_SCALE_4_G);
  // Set by pass mode for the magnetometers
  I2CwriteByte(MPU9250_ADDRESS,0x37,0x02);
  
  // Request continuous magnetometer measurements in 16 bits
  I2CwriteByte(MAG_ADDRESS,0x0A,0x16);
  
   pinMode(13, OUTPUT);
  Timer1.initialize(10000);         // initialize timer1, and set a 1/2 second period
  Timer1.attachInterrupt(callback);  // attaches callback() as a timer overflow interrupt

  ti=millis();


}
void loop() {
  
  btIrOlvas();
  sonarOlvas();
  nemtomEzMi();    
  
  IrOlvas();
  if(bekapcsolva){
    if(state == 0){
      kikapcs();
    }
    else if(state == 1){
      kovet();
    }
    else if(state == 2){
      dobozKovet();
    }
    else if(state == 3){
      meghajt();
    }
  }
}

void dobozKovet(){
 switch(magnitudeGep){
  case 0: 
  if ( akadaly == 0 || akadaly > 25){
    elsoCelIrany = celIrany;
    setMagniError();
  }
  else if(akadaly < 25){
    keneOlvasni = false;
    magnitudeGep = 1;
    setCelIrany(90); //mondjuk ez a bal?
    endTime = millis();
    stateChangeTime = millis();
  }
  break;

  case 1:
  if (stateChangeTime + 3000 > millis()){
    setMagniError();
  } else {
    celIrany = elsoCelIrany;
    magnitudeGep = 2;
    stateChangeTime = millis();
  }
  break;

  case 2:
  if (stateChangeTime + (endTime - startTime)* 2 > millis()){
    setMagniError();
  } else{
    setCelIrany(-90);
    magnitudeGep = 3;
    stateChangeTime = millis();
  }
  break;

  case 3:
  if (stateChangeTime + 3500 > millis()){
    setMagniError();
  } else {
    celIrany = elsoCelIrany;
    magnitudeGep = 4;
    stateChangeTime = millis();
  }
  break;
  case 4:
    setMagniError();
  break;
 }
  magniMeghajt();
}

void magniMeghajt(){
  double actualError = Kp * errorMagnitude + Kd*(errorMagnitude-errorMagnitudeLast);
    balMotor(63 - actualError);
    jobbMotor(63 + actualError);
}

void setCelIrany(int rotate){
  celIrany += rotate;
    if(celIrany  > 360){
      celIrany -= 360;
    }
    if(celIrany < 0){
      celIrany += 360;
    }
}

void setMagniError(){
  errorMagnitudeLast = errorMagnitude;
  if (dirInt + 180 > celIrany){
  errorMagnitude = celIrany - (dirInt + 360);
  } 
  if (dirInt - 180 > celIrany){
    errorMagnitude = (celIrany + 360) - dirInt;
  } else{
    errorMagnitude = celIrany - dirInt;
  }
}


void btIrOlvas(){
  if(lastSend + 100 < millis()){
  lastSend += 100;

    Serial2.print("#");
    Serial2.print("a");
    Serial2.print(bekapcsolva);
    
    Serial2.print("b");
    Serial2.print(error);
    
    Serial2.print("c");
    Serial2.print(errorLast);
    
    Serial2.print("d");
    Serial2.print(bitRead(prevMain,4));
    Serial2.print(bitRead(prevMain,3));
    Serial2.print(bitRead(prevMain,2));
    Serial2.print(bitRead(prevMain,1));
    Serial2.print(bitRead(prevMain,0));
    Serial2.println(prevMain);
    
    Serial2.print("e");
    Serial2.print(balraVolt);
    
    Serial2.print("f");
    Serial2.print(jobbraVolt);
    
    Serial2.print("g");
    Serial2.print(akadaly);
    
    Serial2.print("h");
    Serial2.print(bitRead(sensorState,4));
    Serial2.print(bitRead(sensorState,3));
    Serial2.print(bitRead(sensorState,2));
    Serial2.print(bitRead(sensorState,1));
    Serial2.print(bitRead(sensorState,0));

    Serial2.print("~");
}

if(Serial2.available()){
    char data = Serial2.read();

    switch (data){
      case 'l':
        setBTLSpeed();
      break;
      case 'r':
        setBTRSpeed();
      break;
      case 'm':
        state = 3;
      break;
      case 'n':
        state = 0;
      break;
      default:
      rpmBT*=10;
      rpmBT += data - '0';
      break;
    }
  }
}

void callback()
{ 
  intFlag=true;
  digitalWrite(13, digitalRead(13) ^ 1);
}

// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}

void setBTRSpeed(){
  if(rpmBT < 140 ){
    jobbMotor(rpmBT);
  }
  rpmBT = 0;
}

void setBTLSpeed(){
  if(rpmBT < 140 ){
    balMotor(rpmBT);
  }
  rpmBT = 0;
}



void kovet(){
  errorLast = error;
  switch (sensorState){
    case B00001: error = 2.5; prevMain = 1;  break; //8
    case B00010: 
      error = 2.5; 
      prevMain = 2;
      if(mintatTalal(sensorState)){
        jobbraVolt = false; 
        balraVolt = false; break; //8
      }
      break;
        
    case B00100: 
      error = 0; 
      prevMain = 4; 
        jobbraVolt = false; 
        balraVolt = false; break; //8
    break;  //4
    
    case B01000: 
      error = -2.5; 
      prevMain = 8; 
      if(mintatTalal(sensorState)){
        jobbraVolt = false; 
        balraVolt = false; break; //8
      }
   
    break; //8
    
    case B10000: error = -2.5; prevMain = 16;  break; //16
    
    case B00011: error = 2.5; savValtEllenoriz(); break;  //3
    case B00110: error = 0; savValtEllenoriz(); break;  //6
    case B01100: error = 0; savValtEllenoriz(); break; //12
    case B11000: error = -2.5; savValtEllenoriz(); break; //24
    
    case B00111:
    case B01110:
    error = 1;
    case B11100:
    savValtEllenoriz();
    break;
    
    
    case B01111:
    case B11110:
    case B11111: 
        error = 0;
        kereszt = millis();  
    break; 

    case B00000:
    if(mintatTalal(sensorState)){
      uresVan();
    }

    break;
    default:
    error = 1.3;
    break;
  }
  if(bekapcsolva){
    meghajt();
  }
}
void savValtEllenoriz(){
  if(mintatTalal(sensorState)){
    if(jobbraVoltJelzes + savValtIdo > millis()){
      jobbraVoltJelzes = millis();
    } 
    else if (balraVoltJelzes + savValtIdo > millis()) {
        balraVoltJelzes = millis();
    }
    else if(sensorState == B00011 && prevMain == B00001 && error <0){
      jobbraVoltJelzes = millis();
    }
    else if(sensorState == B11000 && prevMain == B10000 && error > 0){
      balraVoltJelzes = millis();
    }
    else if (sensorState - prevMain < prevMain){
      if(szuro[1] == 4 && szuro[2] == 4 && error < 0){
        balraVoltJelzes = millis();  
      }else{
        jobbraVoltJelzes = millis();
      }
    }
    else if (sensorState - prevMain > prevMain){
      if(szuro[1] == 4 && szuro[2] == 4  && error > 0){
        jobbraVoltJelzes = millis();
      } else{
        balraVoltJelzes = millis();  
      }
    }else if(error < 0 || errorLast < 0){
        jobbraVoltJelzes = millis();  
    } else {
      balraVoltJelzes = millis();
    }
  }
}


void uresVan(){
  if(jobbraVoltJelzes + savValtIdo > millis() ){
    errorLast = error;
    error = savValtEro*0.62;
    jobbraVolt = true;
  } else if (balraVoltJelzes + savValtIdo > millis() ){
    errorLast = error;
    error = -savValtEro*0.4;
    balraVolt = true;

  } else if(kereszt + 1500 > millis() && !balraVolt && !jobbraVolt){
    kikapcs();
    state = 0;
  }
}

void kikapcs(){
  analogWrite(A_MotorEnable, 0); //jobb
  analogWrite(B_MotorEnable, 0);  //bal
  digitalWrite(A_MotorDir1, LOW);
  digitalWrite(A_MotorDir2, LOW);
  digitalWrite(B_MotorDir1, LOW);
  digitalWrite(B_MotorDir2, LOW);
  bekapcsolva = false;
  state = 0;
}


void meghajt(){
    double actualError = Kp * error + Kd*(error-errorLast);
    balMotor(53 + actualError);
    jobbMotor(53 - actualError);
  
}

void jobbMotor(int pwm){
  if(pwm > maxPwm){pwm = maxPwm;}
  if(pwm < minPwm) {pwm = 0;}
  analogWrite(A_MotorEnable, pwm); //jobb
  digitalWrite(A_MotorDir1, HIGH);
  digitalWrite(A_MotorDir2, LOW);
  
}

void balMotor(int pwm){
  if(pwm > maxPwm){pwm = maxPwm;}
  if(pwm < minPwm) {pwm = 0;}
  analogWrite(B_MotorEnable, pwm);  //bal
  digitalWrite(B_MotorDir1, HIGH);
  digitalWrite(B_MotorDir2, LOW);
}

void kapcsolas(){
  if (bekapcsolva) {
    state = 0;
    bekapcsolva = false; 
    analogWrite(A_MotorEnable,0);   //Left Motor Speed
    analogWrite(B_MotorEnable,0);  //Right Motor Speed
  }
  else{
    bekapcsolva = true;
    state = 1;
  }
}

void magniKapcsolas(){
  if (bekapcsolva) {
    state = 0;
    bekapcsolva = false; 
    analogWrite(A_MotorEnable,0);   //Left Motor Speed
    analogWrite(B_MotorEnable,0);  //Right Motor Speed
  }
  else{
    startTime = millis();
    bekapcsolva = true;
    state = 2;
  }
}
void sonarOlvas() {
  if (utolsoSonarOlvasas + 200 < millis()) {
    utolsoSonarOlvasas = millis();
    digitalWrite(SONAR_TRIGGER_PIN, LOW);
    delayMicroseconds(2);
  
    digitalWrite(SONAR_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
  
    digitalWrite(SONAR_TRIGGER_PIN, LOW);
    sonarDuration = pulseIn(SONAR_ECHO_PIN, HIGH);
  
    if(sonarDuration >= 11640){ //58.2 * 200(max distance)
      akadaly = 0;
    }
    else {
      sonarDistance = sonarDuration / 58.2;
      akadaly = sonarDistance;
    }
  }   
}

void IrOlvas()
{
  boolean tempSens[5];

  tempSens[0] = analogRead(irPins[0]) > hatarertek ? 1 : 0; //jobb szĂŠlsĹ
  tempSens[1] = analogRead(irPins[1]) > hatarertek ? 1 : 0;
  tempSens[2] = analogRead(irPins[2]) > hatarertek ? 1 : 0; //kozepso
  tempSens[3] = analogRead(irPins[3]) > hatarertek ? 1 : 0;
  tempSens[4] = analogRead(irPins[4]) > hatarertek ? 1 : 0; //bal szĂŠlsĹ

  atalakit(tempSens);
}

void atalakit(boolean tempSens[5]) {
  sum=0;
  sensorState = 0;
  for (int i=0;i<5;i++){
    int temp = tempSens[i];
    sum += temp;
    digitalWrite(LED0 - i, tempSens[i]);
    sensorState = sensorState + (tempSens[i]<<i);
    
  }

  if(szuresIdo + szuresIdoKoz < millis()){
    szuresIdo = millis();
    mintaRogizt(sensorState);
  }
}

void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.endTransmission();
  
  // Read Nbytes
  Wire.requestFrom(Address, Nbytes); 
  uint8_t index=0;
  while (Wire.available())
    Data[index++]=Wire.read();
}

void addAvg(){
  int sum = 0;
  for(int i=4; i> 0; i--){
    sum += avgVal[i-1];
    avgVal[i] = avgVal[i-1];
  }
  sum+=dirInt;
  avgVal[0] = dirInt;

  avgDir = sum /5;
}

int minVal = 500;
int maxVal = -500;

void nemtomEzMi(){
  while (!intFlag);
  intFlag=false;
  
  uint8_t Buf[14];
  I2Cread(MPU9250_ADDRESS,0x3B,14,Buf);
  
  int16_t ax=-(Buf[0]<<8 | Buf[1]);
  int16_t ay=-(Buf[2]<<8 | Buf[3]);
  int16_t az=Buf[4]<<8 | Buf[5];

  int16_t gx=-(Buf[8]<<8 | Buf[9]);
  int16_t gy=-(Buf[10]<<8 | Buf[11]);
  int16_t gz=Buf[12]<<8 | Buf[13];
  
  uint8_t ST1;
  do
  {
    I2Cread(MAG_ADDRESS,0x02,1,&ST1);
  }
  while (!(ST1&0x01));

  uint8_t Mag[7];  
  I2Cread(MAG_ADDRESS,0x03,7,Mag);

  int16_t mx=-(Mag[3]<<8 | Mag[2]);
  int16_t my=-(Mag[1]<<8 | Mag[0]);
  int16_t mz=-(Mag[5]<<8 | Mag[4]);
  
  mx = (mx+344)/ 2.45;
  my = (my+230) / 2.49;

  dir = atan2(my,mx);
  dirInt = dir * 180 /M_PI;
  dirInt += 180;
/*
  if(my > maxVal){
    maxVal = my;
  }
  if(my < minVal){
    minVal = my;
  }

  Serial.print(my);
  Serial.print("\t");
  Serial.print(minVal);
  Serial.print("\t");
  Serial.print(maxVal);
  Serial.print("\t");
  Serial.println();
*/
  addAvg();
  
  if(!bekapcsolva && akadaly != 0){
    celIrany  = avgDir;
    elsoCelIrany = celIrany;
  }
}

