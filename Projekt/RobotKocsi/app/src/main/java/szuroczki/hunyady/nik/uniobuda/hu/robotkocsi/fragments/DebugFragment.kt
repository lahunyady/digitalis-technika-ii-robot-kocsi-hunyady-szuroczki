package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_debug.*
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.R
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.dtos.CarState

class DebugFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_debug, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val carState = CarState()

        tvIsOnVal.text = carState.isOn.toString()
        tvErrorVal.text = carState.error.toString()
        tvPreviousMainVal.text = carState.previousMain.toString()
        tvWasLeftVal.text = carState.wasLeft.toString()
        tvWasRightVal.text = carState.wasRight.toString()
        tvSensorStateVal.text = carState.sensorState.toString()
        tvErrorVal.text = carState.error.toString()

    }
}