package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.device_list.*

class ListActivity : FragmentActivity() {
    companion object {
        const val EXTRA_DEVICE_ADDRESS = "device_address"
    }

    private var mBtAdapter: BluetoothAdapter? = null
    private var mPairedDevicesArrayAdapter: ArrayAdapter<String>? = null
    private var mNewDevicesArrayAdapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.device_list)
        setResult(Activity.RESULT_CANCELED)

        button_scan.setOnClickListener {
            discovery()
            it.visibility = View.GONE
        }
        mPairedDevicesArrayAdapter = ArrayAdapter(this, R.layout.device_name)
        mNewDevicesArrayAdapter = ArrayAdapter(this, R.layout.device_name)
        paired_devices.adapter = mPairedDevicesArrayAdapter
        paired_devices.onItemClickListener = mDeviceClickListener
        new_devices.adapter = mNewDevicesArrayAdapter
        new_devices.onItemClickListener = mDeviceClickListener
        var filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        this.registerReceiver(mReceiver, filter)
        filter = IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        this.registerReceiver(mReceiver, filter)
        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
        mBtAdapter?.bondedDevices?.let {
            if (it.size > 0) {
                title_paired_devices.visibility = View.VISIBLE
                for (device in it) {
                    mPairedDevicesArrayAdapter?.add(device.name + "\n" + device.address)
                }
            } else mPairedDevicesArrayAdapter?.add("No devices")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mBtAdapter != null) {
            mBtAdapter?.cancelDiscovery()
        }
        this.unregisterReceiver(mReceiver)
    }

    private fun discovery() {
        title_new_devices.visibility = View.VISIBLE
        mBtAdapter?.let {
            if (it.isDiscovering) mBtAdapter?.cancelDiscovery()
        }
        mBtAdapter?.startDiscovery()
    }

    private val mDeviceClickListener = AdapterView.OnItemClickListener { _, v, _, _ ->
        mBtAdapter?.cancelDiscovery()
        val info = (v as TextView).text.toString()
        val address = info.substring(info.length - 17)
        val intent = Intent()
        intent.putExtra(EXTRA_DEVICE_ADDRESS, address)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (device.bondState != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter?.add(device.name + "\n" + device.address)
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                if (mNewDevicesArrayAdapter?.count == 0) {
                    mNewDevicesArrayAdapter?.add("Not found")
                }
            }
        }
    }
}