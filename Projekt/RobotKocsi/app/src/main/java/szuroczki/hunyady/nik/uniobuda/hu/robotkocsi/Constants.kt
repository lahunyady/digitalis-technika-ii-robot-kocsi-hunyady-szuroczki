package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi

class Constants {
    companion object {
        // Message types sent from the BluetoothChatService Handler
        const val MESSAGE_READ = 2
        const val MESSAGE_WRITE = 3
        const val MESSAGE_DEVICE_NAME = 4
        const val MESSAGE_TOAST = 5

        const val REQUEST_CONNECT_DEVICE = 1
        const val REQUEST_ENABLE_BT = 2
        const val REQUEST_SCAN_DEVICES = 3

        // Key names received from the BluetoothChatService Handler
        const val DEVICE_NAME = "device_name"
        const val TOAST = "toast"
    }
}