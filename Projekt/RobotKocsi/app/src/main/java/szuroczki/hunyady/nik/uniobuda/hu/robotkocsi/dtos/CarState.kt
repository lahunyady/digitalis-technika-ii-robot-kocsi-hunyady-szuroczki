package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.dtos

import java.util.*
import kotlin.collections.ArrayList

data class CarState(
        var isOn: Boolean = false,
        var error: Double = 0.0,
        var lastError: Double = 0.0,
        var previousMain: ArrayList<Int> = ArrayList(Arrays.asList(0, 0, 0, 0, 0)),
        var wasLeft: Boolean = false,
        var wasRight: Boolean = false,
        var obstacle: Double =0.00,
        var sensorState: ArrayList<Int> = ArrayList(Arrays.asList(0, 0, 0, 0, 0))

) {
    override fun toString(): String {
        return "CarState(isOn=$isOn, error=$error, lastError=$lastError, previousMain=$previousMain, wasLeft=$wasLeft, wasRight=$wasRight, obstacle=$obstacle, sensorState=$sensorState)"
    }
}