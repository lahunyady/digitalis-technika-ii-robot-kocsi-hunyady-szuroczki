package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_lane.view.*
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.R
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.adapter.HistoryAdapter

class HistoryFragment : Fragment() {
        fun add(allHistory: ArrayList<Int>) {
            historyAdapter.add(allHistory)
        }



    var historyAdapter: HistoryAdapter = HistoryAdapter()
    private var recyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lane, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.recyclerViewHistory
        recyclerView!!.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recyclerView!!.adapter = historyAdapter
        add(arrayListOf(0, 0, 0, 0, 0))
    }
}