package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_lane_item.view.*
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.R

class HistoryAdapter : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    private val historySize = 20
    private var sensorHistory: ArrayList<ArrayList<Int>> = ArrayList()

    fun add(sensorState: ArrayList<Int>) {
        this.sensorHistory.add(0, sensorState)

        if (sensorHistory.size > historySize) {
            sensorHistory.removeAt(historySize)
        }
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HistoryViewHolder {
        return HistoryViewHolder(View.inflate(p0.context, R.layout.fragment_lane_item, null))
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, p1: Int) {
        val selectedValue: ArrayList<Int> = sensorHistory[p1]

        var iter = 0
        selectedValue.forEach {
            if (it == 0) holder.historyLinearLayout.getChildAt(iter++).setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.colorWhite))
            else holder.historyLinearLayout.getChildAt(iter++).setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.colorBlack))
        }
    }

    override fun getItemCount(): Int {
        return this.sensorHistory.size
    }

    class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var historyLinearLayout = itemView.historyElement!!
    }


}