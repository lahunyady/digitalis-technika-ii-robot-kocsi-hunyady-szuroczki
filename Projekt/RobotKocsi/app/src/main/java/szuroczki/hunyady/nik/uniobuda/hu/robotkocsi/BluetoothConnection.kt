package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.os.Bundle
import android.os.Handler
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class BluetoothConnection(handler: Handler) {
    companion object {
        const val STATE_NONE = 0       // we're doing nothing
        const val STATE_LISTEN = 1     // now listening for incoming connections
        const val STATE_CONNECTING = 2 // now initiating an outgoing connection
        const val STATE_CONNECTED = 3  // now connected to a remote device
    }

    private var mAdapter: BluetoothAdapter? = null
    private var mHandler: Handler? = null
    private var mAcceptThread: AcceptThread? = null
    private var mConnectThread: ConnectThread? = null
    private var mConnectedThread: ConnectedThread? = null
    private var mState: Int = 0
    private var mNewState: Int = 0

    private val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private val NAME = "RobotRemote"

    init {
        mAdapter = BluetoothAdapter.getDefaultAdapter()
        mState = STATE_NONE
        mNewState = mState
        mHandler = handler
    }

    @Synchronized
    fun getState() = mState

    @Synchronized
    fun start() {
        mConnectThread?.cancel()
        mConnectThread = null

        mConnectedThread?.cancel()
        mConnectedThread = null

        if (mAcceptThread == null) {
            mAcceptThread = AcceptThread()
            mAcceptThread?.start()
        }
    }

    @Synchronized
    fun connect(device: BluetoothDevice?) {

        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread?.cancel()
                mConnectThread = null
            }
        }

        mConnectedThread?.cancel()
        mConnectedThread = null

        mConnectThread = ConnectThread(device)
        mConnectThread?.start()

    }

    @Synchronized
    fun connected(socket: BluetoothSocket?, device: BluetoothDevice?) {
        mConnectThread?.cancel()
        mConnectThread = null

        mConnectedThread?.cancel()
        mConnectedThread = null

        mAcceptThread?.cancel()
        mAcceptThread = null

        mConnectedThread = ConnectedThread(socket)
        mConnectedThread?.start()

        val msg = mHandler?.obtainMessage(Constants.MESSAGE_DEVICE_NAME)
        val bundle = Bundle()
        bundle.putString(Constants.DEVICE_NAME, device?.name)
        msg?.data = bundle
        mHandler?.sendMessage(msg)
    }

    @Synchronized
    fun stop() {
        if (mConnectThread != null) {
            mConnectThread?.cancel()
            mConnectThread = null
        }

        if (mConnectedThread != null) {
            mConnectedThread?.cancel()
            mConnectedThread = null
        }

        if (mAcceptThread != null) {
            mAcceptThread?.cancel()
            mAcceptThread = null
        }
        mState = STATE_NONE
    }

    fun write(out: ByteArray) {
        var r: ConnectedThread? = null
        synchronized(this) {
            if (mState != STATE_CONNECTED) return
            r = mConnectedThread
        }
        r?.write(out)
    }

    private fun connectionFailed() {
        val msg = mHandler?.obtainMessage(Constants.MESSAGE_TOAST)
        val bundle = Bundle()
        bundle.putString(Constants.TOAST, "Unable to connect device")
        msg?.data = bundle
        mHandler?.sendMessage(msg)

        mState = STATE_NONE
        this@BluetoothConnection.start()
    }

    private fun connectionLost() {
        val msg = mHandler?.obtainMessage(Constants.MESSAGE_TOAST)
        val bundle = Bundle()
        bundle.putString(Constants.TOAST, "Device connection was lost")
        msg?.data = bundle
        mHandler?.sendMessage(msg)

        mState = STATE_NONE
        this@BluetoothConnection.start()
    }

    private inner class AcceptThread : Thread() {
        private val mmServerSocket: BluetoothServerSocket?

        init {
            var tmp: BluetoothServerSocket? = null
            try {
                tmp = mAdapter?.listenUsingInsecureRfcommWithServiceRecord(NAME, MY_UUID)
            } catch (e: IOException) {
            }

            mmServerSocket = tmp
            mState = STATE_LISTEN
        }

        override fun run() {
            name = "AcceptThread"

            var socket: BluetoothSocket?

            while (mState != STATE_CONNECTED) {
                try {
                    socket = mmServerSocket?.accept()
                } catch (e: IOException) {
                    break
                }

                if (socket != null) {
                    synchronized(this@BluetoothConnection) {
                        when (mState) {
                            STATE_LISTEN, STATE_CONNECTING -> connected(socket, socket.remoteDevice)
                            STATE_NONE, STATE_CONNECTED ->
                                try {
                                    socket.close()
                                } catch (e: IOException) {
                                }
                        }
                    }
                }
            }
        }

        fun cancel() = try {
            mmServerSocket?.close()
        } catch (e: IOException) {
        }

    }

    private inner class ConnectThread(private val mmDevice: BluetoothDevice?) : Thread() {
        private val mmSocket: BluetoothSocket?

        init {
            var tmp: BluetoothSocket? = null

            try {
                tmp = mmDevice?.createInsecureRfcommSocketToServiceRecord(
                        MY_UUID)
            } catch (e: IOException) {
            }

            mmSocket = tmp
            mState = STATE_CONNECTING
        }

        override fun run() {

            name = "ConnectThread"
            mAdapter?.cancelDiscovery()

            try {
                mmSocket?.connect()
            } catch (e: IOException) {
                try {
                    mmSocket?.close()
                } catch (e2: IOException) {
                }

                connectionFailed()
                return
            }

            synchronized(this@BluetoothConnection) {
                mConnectThread = null
            }

            connected(mmSocket, mmDevice)
        }

        fun cancel() = try {
            mmSocket?.close()
        } catch (e: IOException) {
        }
    }

    private inner class ConnectedThread(private val mmSocket: BluetoothSocket?) : Thread() {
        private val mmInStream: InputStream?
        private val mmOutStream: OutputStream?

        init {
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null

            try {
                tmpIn = mmSocket?.inputStream
                tmpOut = mmSocket?.outputStream
            } catch (e: IOException) {
            }

            mmInStream = tmpIn
            mmOutStream = tmpOut
            mState = STATE_CONNECTED
        }

        override fun run() {
            val buffer = ByteArray(1024)
            var bytes: Int

            while (mState == STATE_CONNECTED) {
                try {
                    bytes = mmInStream?.read(buffer) ?: 0
                    mHandler?.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)?.sendToTarget()
                } catch (e: IOException) {
                    connectionLost()
                    break
                }

            }
        }

        fun write(buffer: ByteArray) {
            try {
                mmOutStream?.write(buffer)
                mHandler?.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer)?.sendToTarget()
            } catch (e: IOException) {
            }

        }

        fun cancel() = try {
            mmSocket?.close()
        } catch (e: IOException) {
        }
    }
}