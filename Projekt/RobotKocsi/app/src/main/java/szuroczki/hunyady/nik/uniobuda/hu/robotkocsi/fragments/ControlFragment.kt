package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import io.github.controlwear.virtual.joystick.android.JoystickView
import kotlinx.android.synthetic.main.fragment_control.*
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.R
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.bluetooth.BluetoothParser

class ControlFragment : Fragment() {
    lateinit var joystickView: JoystickView
    private lateinit var switch: SwitchCompat

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_control, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        joystickView = joystick
        switch = sIsOn

        switch.setOnCheckedChangeListener { buttonView, isChecked ->
            BluetoothParser.setManualMode(isChecked)
            joystickView.isEnabled = isChecked
        }
        joystickView.setOnMoveListener({ angle, strength -> BluetoothParser.setSpeed(angle, strength) }, 50)
    }
}


