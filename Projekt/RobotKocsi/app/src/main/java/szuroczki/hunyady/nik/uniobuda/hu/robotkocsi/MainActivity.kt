package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi


import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_debug.*
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.bluetooth.BluetoothParser
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.fragments.HistoryFragment

class MainActivity : AppCompatActivity() {

    companion object {

        var connection: BluetoothConnection? = null
        var buffer: StringBuffer? = null

        const val MESSAGE_READ = 2
        const val MESSAGE_WRITE = 3
        const val MESSAGE_DEVICE_NAME = 4
        const val MESSAGE_TOAST = 5

        const val TOAST = "toast"

        fun sendMessage(message: String) {
            if (connection?.getState() != BluetoothConnection.STATE_CONNECTED) {
                return
            }
            if (message.isNotEmpty()) {
                val send = message.toByteArray()
                connection?.write(send)
                buffer?.setLength(0)
            }
        }
    }

    private lateinit var bluetoothAdapter: BluetoothAdapter

    private var stringBuilder: StringBuilder = StringBuilder()

    var connectedDeviceName = ""


    private fun scanDevices() {
        val intent = Intent(this, ListActivity::class.java)
        startActivityForResult(intent, Constants.REQUEST_CONNECT_DEVICE)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (this.bluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            finish()
        }

        fab.setOnClickListener {
            scanDevices()
        }
    }


    private fun setupConnection() {
        connection = BluetoothConnection(handler)
        buffer = StringBuffer("")
    }


    override fun onStart() {
        super.onStart()
        if (!this.bluetoothAdapter.isEnabled) {
            val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(intent, Constants.REQUEST_ENABLE_BT)
        } else if (connection == null) setupConnection()
    }

    override fun onResume() {
        super.onResume()

        connection?.let {
            if (it.getState() == BluetoothConnection.STATE_NONE)
                connection?.start()
        }
    }


    private var handler = object : Handler() {
        override fun handleMessage(msg: android.os.Message?) {
            when (msg?.what) {
                MESSAGE_WRITE -> {
                    val write = msg?.obj as ByteArray
                    val message = String(write)

                }
                MESSAGE_READ -> {
                    val read = msg?.obj as ByteArray
                    val message = String(read, 0, msg?.arg1)
                    resolve(message);
                }
                MESSAGE_DEVICE_NAME -> {
                    connectedDeviceName = msg?.data.getString(Constants.DEVICE_NAME)
                    Toast.makeText(applicationContext, "Connected to $connectedDeviceName", Toast.LENGTH_SHORT).show()

                }
                MESSAGE_TOAST -> Toast.makeText(applicationContext, msg?.data.getString(TOAST), Toast.LENGTH_SHORT).show()
            }
            onResume()
        }
    }

    @Synchronized
    private fun resolve(message: String) {
        if (message.contains("~")) {
            stringBuilder.append(message.subSequence(0, message.indexOf("~")))
            if (BluetoothParser.parse(stringBuilder.toString())) {
                refreshFragmentData()
            }
            stringBuilder.setLength(0)
        }
        if (message.contains("#")) {
            stringBuilder.setLength(0)
            if (message.indexOf("#") + 1 != message.lastIndex + 1)
                stringBuilder.append(message.subSequence(message.indexOf("#") + 1, message.lastIndex + 1))

        }
        if (!message.contains("#") && !message.contains("~")) {
            stringBuilder.append(message)
        }
    }

    private fun refreshFragmentData() {
        fragmentDebug.tvErrorVal.text = BluetoothParser.carState.error.toString()

        fragmentDebug.tvSensorStateVal.text = BluetoothParser.carState.sensorState.toString()
        fragmentDebug.tvWasRightVal.text = BluetoothParser.carState.wasRight.toString()
        fragmentDebug.tvPreviousMainVal.text = BluetoothParser.carState.previousMain.toString()
        fragmentDebug.tvIsOnVal.text = BluetoothParser.carState.isOn.toString()
        fragmentDebug.tvLastErrorVal.text = BluetoothParser.carState.lastError.toString()
        fragmentDebug.tvWasLeftVal.text = BluetoothParser.carState.wasLeft.toString()
        fragmentDebug.tvObstacleVal.text  =BluetoothParser.carState.obstacle.toString()


        val historyFragment: HistoryFragment = supportFragmentManager.findFragmentById(R.id.fragmentHistory) as HistoryFragment
        historyFragment.add(BluetoothParser.carState.sensorState)
        historyFragment.historyAdapter.notifyDataSetChanged()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Constants.REQUEST_CONNECT_DEVICE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val address = data?.extras?.getString(ListActivity.EXTRA_DEVICE_ADDRESS)
                    val device = bluetoothAdapter.getRemoteDevice(address)
                    connection?.connect(device)
                }
            }
            Constants.REQUEST_ENABLE_BT -> {
                if (resultCode == Activity.RESULT_OK) setupConnection()
                else finish()
            }
            Constants.REQUEST_SCAN_DEVICES -> {
                if (resultCode == Activity.RESULT_OK) {
                }
            }
        }
    }


}