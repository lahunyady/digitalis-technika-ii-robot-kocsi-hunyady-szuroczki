package szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.bluetooth

import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.MainActivity
import szuroczki.hunyady.nik.uniobuda.hu.robotkocsi.dtos.CarState

class BluetoothParser {
    companion object {
        var carState: CarState = CarState()

        fun setSpeed(angle: Int, strength: Int) {
            var lRPM = 110
            var rRPM = 110
            when {
                angle < 90 -> rRPM -= (90 - angle)
                angle < 180 -> lRPM -= 90 - (180 - angle)
                else -> {
                    lRPM = 0
                    rRPM = 0
                }
            }
            if (angle != 0 && strength != 0) {
                MainActivity.sendMessage(lRPM.toString() + "l")
                MainActivity.sendMessage(rRPM.toString() + "r")
            }
        }

        fun setManualMode(isManual: Boolean) {
            if(isManual)
                MainActivity.sendMessage("m")
            else
                MainActivity.sendMessage("n")
        }


        fun parse(toParse: String): Boolean {
            println("Message2" + toParse)
            if (formatCheck(toParse)) {
                val message = toParse.substring(toParse.indexOf('a'), toParse.indexOf('h') + 6)
                println("Message" + message)

                carState.isOn = message[message.indexOf('a') + 1] == '1'
                carState.error = message.substring(message.indexOf('b') + 1, message.indexOf('c')).toDouble()
                carState.lastError = message.substring(message.indexOf('c') + 1, message.indexOf('d')).toDouble()

                val toInt = ArrayList<Int>()
                toInt.add(message[message.indexOf('d') + 1].toString().toInt())
                toInt.add(message[message.indexOf('d') + 2].toString().toInt())
                toInt.add(message[message.indexOf('d') + 3].toString().toInt())
                toInt.add(message[message.indexOf('d') + 4].toString().toInt())
                toInt.add(message[message.indexOf('d') + 5].toString().toInt())

                carState.previousMain = toInt
                carState.wasLeft = message[message.indexOf('e') + 1] == '1'
                carState.wasRight = message[message.indexOf('f') + 1] == '1'

                carState.obstacle = message.substring(message.indexOf('g') + 1, message.indexOf('h')).toDouble()

                val ss = ArrayList<Int>()
                ss.add(message[message.indexOf('h') + 1].toString().toInt())
                ss.add(message[message.indexOf('h') + 2].toString().toInt())
                ss.add(message[message.indexOf('h') + 3].toString().toInt())
                ss.add(message[message.indexOf('h') + 4].toString().toInt())
                ss.add(message[message.indexOf('h') + 5].toString().toInt())
                carState.sensorState = ss

                return true

            }
            return false
        }

        private fun formatCheck(message: String): Boolean {
            if (message.length > 37) return false
            if (message.length < 32) return false
            if (!message.contains('a')) return false
            if (message[0] != 'a') return false
            if (!message.contains('b')) return false
            if (!message.contains('c')) return false
            if (!message.contains('d')) return false
            if (!message.contains('e')) return false
            if (!message.contains('f')) return false
            if (!message.contains('g')) return false
            if (!message.contains('h')) return false
            if (message.indexOf('h') + 5 >= message.length) return false
            return true
        }
    }
}